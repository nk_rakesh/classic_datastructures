
package com.rk.queue.impl;

import com.rk.queue.Queue;

/**
 * Implementation of the queue ADT using a fixed-length array. All operations
 * are performed in constant time. An exception is thrown if an enqueue
 * operation is attempted when the size of the queue is equal to the length of
 * the array.
 *
 * 
 */

public class ArrayQueue<E> implements Queue<E> {

	public static final int DEFAULT_CAPACITY = 1000;
	private E[] internalDataArray;

	private int topElementIndex = 0;

	private int noOfElements = 0;

	public ArrayQueue() {
		this(DEFAULT_CAPACITY);
	}

	@SuppressWarnings({ "unchecked" })
	public ArrayQueue(int capacity) {
		internalDataArray = (E[]) new Object[capacity];
	}

	@Override
	public int size() {
		return noOfElements;
	}

	@Override
	public boolean isEmpty() {
		return (noOfElements == 0);
	}

	@Override
	public void enqueue(E e) throws IllegalStateException {
		if (noOfElements == internalDataArray.length)
			throw new IllegalStateException("Queue is full");
		int avail = (topElementIndex + noOfElements) % internalDataArray.length;
		internalDataArray[avail] = e;
		noOfElements++;
	}

	@Override
	public E first() {
		if (isEmpty())
			return null;
		return internalDataArray[topElementIndex];
	}

	@Override
	public E dequeue() {
		if (isEmpty())
			return null;
		E answer = internalDataArray[topElementIndex];
		internalDataArray[topElementIndex] = null; // dereference to help
													// garbage collection
		topElementIndex = (topElementIndex + 1) % internalDataArray.length;
		noOfElements--;
		return answer;
	}

	/**
	 * Returns a string representation of the queue as a list of elements. This
	 * method runs in O(n) time, where n is the size of the queue.
	 * 
	 * @return textual representation of the queue.
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder("(");
		int k = topElementIndex;
		for (int j = 0; j < noOfElements; j++) {
			if (j > 0)
				sb.append(", ");
			sb.append(internalDataArray[k]);
			k = (k + 1) % internalDataArray.length;
		}
		sb.append(")");
		return sb.toString();
	}
}
