
package com.rk.queue;

/**
 * Interface for a queue: a collection of elements that are inserted and removed
 * according to the first-in first-out principle. Although similar in purpose,
 * this interface differs from java.util.Queue.
 *
 */
public interface Queue<E> {
	void enqueue(E e);

	E dequeue();

	E first();

	int size();

	boolean isEmpty();
}
