
package com.rk.stack.impl;

import java.util.LinkedList;

import com.rk.stack.Stack;

public class LinkedStack<E> implements Stack<E> {

	private LinkedList<E> list = new LinkedList<>();

	public LinkedStack() {
	}

	@Override
	public int size() {
		return list.size();
	}

	@Override
	public boolean isEmpty() {
		return list.isEmpty();
	}

	@Override
	public void push(E element) {
		list.addFirst(element);
	}

	@Override
	public E top() {
		return list.peekFirst();
	}

	@Override
	public E pop() {
		return list.removeFirst();
	}

	public String toString() {
		return list.toString();
	}
}
