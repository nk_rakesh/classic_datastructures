
package com.rk.stack.impl;

import com.rk.stack.Stack;

public class ArrayStack<E> implements Stack<E> {
	public static final int DEFAULT_CAPACITY = 1000;
	private E[] internalArray;
	private int topElementIndex = -1;

	public ArrayStack() {
		this(DEFAULT_CAPACITY);
	}

	@SuppressWarnings({ "unchecked" })
	public ArrayStack(int capacity) {
		internalArray = (E[]) new Object[capacity];
	}

	@Override
	public int size() {
		return (topElementIndex + 1);
	}

	@Override
	public boolean isEmpty() {
		return (topElementIndex == -1);
	}

	@Override
	public void push(E e) throws IllegalStateException {
		if (size() == internalArray.length)
			throw new IllegalStateException("Stack is full");
		internalArray[++topElementIndex] = e;
	}

	@Override
	public E top() {
		if (isEmpty())
			return null;
		return internalArray[topElementIndex];
	}

	@Override
	public E pop() {
		if (isEmpty())
			return null;
		E answer = internalArray[topElementIndex];
		internalArray[topElementIndex--] = null;
		return answer;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder("(");
		for (int j = topElementIndex; j >= 0; j--) {
			sb.append(internalArray[j]);
			if (j > 0)
				sb.append(", ");
		}
		sb.append(")");
		return sb.toString();
	}

	/** Demonstrates sample usage of a stack. */
	public static void main(String[] args) {
		Stack<Integer> S = new ArrayStack<>(); // contents: ()
		S.push(5); // contents: (5)
		S.push(3); // contents: (5, 3)
		System.out.println(S.size()); // contents: (5, 3) outputs 2
		System.out.println(S.pop()); // contents: (5) outputs 3
		System.out.println(S.isEmpty()); // contents: (5) outputs false
		System.out.println(S.pop()); // contents: () outputs 5
		System.out.println(S.isEmpty()); // contents: () outputs true
		System.out.println(S.pop()); // contents: () outputs null
		S.push(7); // contents: (7)
		S.push(9); // contents: (7, 9)
		System.out.println(S.top()); // contents: (7, 9) outputs 9
		S.push(4); // contents: (7, 9, 4)
		System.out.println(S.size()); // contents: (7, 9, 4) outputs 3
		System.out.println(S.pop()); // contents: (7, 9) outputs 4
		S.push(6); // contents: (7, 9, 6)
		S.push(8); // contents: (7, 9, 6, 8)
		System.out.println(S.pop()); // contents: (7, 9, 6) outputs 8
	}
}
